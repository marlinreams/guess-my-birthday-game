from random import randint


#prompt user "Hi! what is your name?"

person = input("Hi! what is your name? ")

#guesses between 1924 - 2004

#Try to guess their birth month and year with a prompt fomatted like
# Guess <<guess number>> : <<name>> were you born in <<m>> / <<yyyy>>? then prompts with
#"yes or no?""
# guess = input(f"Guess 1: {person} were you born in {months} / {years} yes or no? ").lower()
# print(guess)
#If computer guesses right print "I knew it!" and stops guessing

#If computer guesses incorrectly print "Drat! Lemme ty again!" @ 5th guess
#print "I have other things to do. Good bye"


i = 5
while i > 0:
    months = randint(1,12)
    years = randint(1924, 2004)
    guess = input(f"Guess 1: {person} were you born in {months} / {years} yes or no? ").lower()
    i -= 1
    if guess =="yes":
        print("I knew it!")
        exit()
    elif guess =="no" and i > 1:
        print("Drat! Lemme try again!")
else:
    print("I have other things to do. Good bye.")
